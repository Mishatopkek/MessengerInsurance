# MessengerInsurence

MessengerInsurence is a Telegram bot written in C# that leverages OCR and ChatGPT to generate insurance based on photos of your passport and car. The bot offers a seamless way to interact and get insurance through Telegram.

## Table of Contents

- [Description](#description)
- [Features](#features)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Dependencies](#dependencies)
- [Contributing](#contributing)
- [Contact](#contact)
- [Demo Video](#demo-video)

## Description

MessengerInsurence is designed to automate the insurance generation process. It uses OCR to extract information from photos of your passport and car and then utilizes ChatGPT to generate the insurance policy. This bot simplifies the entire process, making it quick and user-friendly.

### Setup Instructions and Dependencies

1. **Setup Instructions:**
    - Download the .NET project.
    - Ensure you have .NET 8 installed.
    - Publish the project using .NET CLI.

2. **Dependencies:**
    - Mindee
    - Telegram.Bot

### Detailed Description of the Bot Workflow

1. **User Interaction:**
    - The user sends photos of their passport and car to the bot via Telegram.
    
2. **Data Extraction:**
    - The bot uses Mindee's OCR capabilities to extract relevant information from the photos.
    
3. **Insurance Generation:**
    - Using the extracted information, ChatGPT generates a customized insurance policy.
    
4. **Response:**
    - The bot sends the generated insurance policy back to the user in the Telegram chat.

### Examples of Interaction Flows with the Bot

1. **Start Interaction:**
    - User: `/start`
    - Bot: `Hello! I'm here to assist you with car insurance purchases. How can I help you today?`

2. **Sending Photos:**
    - User sends a photo of their passport.
    - Bot: `Got it! Now, please send me a photo of your car.`

3. **Generating Insurance:**
    - User sends a photo of their car.
    - Bot: `It is now generates policy, it usually takes about a minute, please wait...`
    - Bot: `Here is your generated insurance policy: [insurance details]`

## Features

- Generate insurance based on photos of passport and car.
- Utilizes OCR for data extraction.
- Uses ChatGPT to create insurance policies.
- Easily configurable architecture for custom bot enhancements.

## Installation

To install and run the bot:

1. Download the .NET project.
2. Ensure you have .NET 8 installed on your machine.
3. Publish the project using the .NET CLI:
    ```bash
    dotnet publish -c Release
    ```
4. Run the published project:
    ```bash
    dotnet run
    ```

## Configuration

To successfully run your .NET application, you need to set several environment variables before starting the application. These variables include:

- `MINDEE_API_KEY`
- `OPENAI_API_KEY`
- `TELEGRAM_API_KEY`

Here's how you can set these environment variables on different operating systems:

### Windows

On Windows, you can use the `set` command to set environment variables for the duration of the command session:

```cmd
set MINDEE_API_KEY=your_mindee_api_key
set OPENAI_API_KEY=your_openai_api_key
set TELEGRAM_API_KEY=your_telegram_api_key
dotnet run
```

Alternatively, you can set the environment variables inline with the `dotnet run` command:

```cmd
set MINDEE_API_KEY=your_mindee_api_key && set OPENAI_API_KEY=your_openai_api_key && set TELEGRAM_API_KEY=your_telegram_api_key && dotnet run
```

### macOS and Linux

On macOS and Linux, you can set environment variables inline with the `dotnet run` command using the `export` command:

```sh
export MINDEE_API_KEY=your_mindee_api_key
export OPENAI_API_KEY=your_openai_api_key
export TELEGRAM_API_KEY=your_telegram_api_key
dotnet run
```

Or you can set the environment variables inline with the `dotnet run` command in a single line:

```sh
MINDEE_API_KEY=your_mindee_api_key OPENAI_API_KEY=your_openai_api_key TELEGRAM_API_KEY=your_telegram_api_key dotnet run
```

### Examples

#### Windows Command Line

```cmd
set MINDEE_API_KEY=abc123
set OPENAI_API_KEY=def456
set TELEGRAM_API_KEY=ghi789
dotnet run
```

Or:

```cmd
set MINDEE_API_KEY=abc123 && set OPENAI_API_KEY=def456 && set TELEGRAM_API_KEY=ghi789 && dotnet run
```

#### macOS/Linux Terminal

```sh
export MINDEE_API_KEY=abc123
export OPENAI_API_KEY=def456
export TELEGRAM_API_KEY=ghi789
dotnet run
```

Or:

```sh
MINDEE_API_KEY=abc123 OPENAI_API_KEY=def456 TELEGRAM_API_KEY=ghi789 dotnet run
```

By using these commands, you can set the necessary environment variables on the fly and run your .NET application with the required API keys.

## Usage

MessengerInsurence has a robust architecture, allowing for easy customization and extension. You can configure your custom bot settings and functionalities by modifying the existing codebase.

## Dependencies

- [Mindee](https://www.mindee.com/)
- [Telegram.Bot](https://github.com/TelegramBots/Telegram.Bot)

## Contributing

If you'd like to contribute to MessengerInsurence, please fork the repository and create a pull request with your changes. Ensure your code follows the existing style and passes all tests.

## Contact

For any inquiries or support, please contact:

- **Email:** [bezuhlyi.mykhailo@gmail.com](mailto:bezuhlyi.mykhailo@gmail.com)
- **LinkedIn:** [Mykhailo Bezuhlyi](https://www.linkedin.com/in/mishatopkek/)

## Demo Video

To see MessengerInsurence in action, check out this demo video:

[![MessengerInsurence Demo](https://img.youtube.com/vi/6671yDYX9pQ/0.jpg)](https://www.youtube.com/watch?v=6671yDYX9pQ)

---

*Note: This project does not currently have a license.*
