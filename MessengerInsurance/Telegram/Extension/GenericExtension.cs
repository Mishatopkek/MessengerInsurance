﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace MessengerInsurance.Telegram.Extension;

/// <summary>
///     Provides extension methods for the generic classes.
/// </summary>
public static class GenericExtension
{
    /// <summary>
    /// Patches the target object with non-default values from the source object.<br/>
    /// ! ChatGPT generated code !
    /// </summary>
    /// <typeparam name="T">The type of the objects being patched.</typeparam>
    /// <param name="target">The target object to be patched.</param>
    /// <param name="source">The source object containing the new values.</param>
    public static T Patch<T>([NotNull] this T target, [NotNull] T source)
    {
        PatchObject(target, source);
        return target;
    }

    private static void PatchObject(object target, object source)
    {
        Type type = target.GetType();
        foreach (PropertyInfo property in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
        {
            if (!property.CanRead || !property.CanWrite) continue;

            object? sourceValue = property.GetValue(source, null);
            object? targetValue = property.GetValue(target, null);

            if (sourceValue == null || sourceValue.Equals(targetValue)) continue;

            if (IsSimpleType(property.PropertyType) || targetValue == null ||
                targetValue.GetType() != sourceValue.GetType())
            {
                property.SetValue(target, sourceValue, null);
            }
            else if (property.PropertyType.IsClass)
            {
                targetValue = Activator.CreateInstance(property.PropertyType);
                property.SetValue(target, targetValue, null);

                PatchObject(targetValue, sourceValue);
            }
        }
    }

    private static bool IsSimpleType(Type type)
    {
        return type.IsPrimitive || type.IsValueType || type == typeof(string);
    }
}