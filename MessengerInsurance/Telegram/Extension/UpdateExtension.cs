﻿using Telegram.Bot.Types;

namespace MessengerInsurance.Telegram.Extension;

/// <summary>
///     Provides extension methods for the Update class.
/// </summary>
public static class UpdateExtension
{
    /// <summary>
    ///     Retrieves the user chat ID from an Update object.
    /// </summary>
    /// <param name="update">The Update object from which to retrieve the chat ID.</param>
    /// <returns>The chat ID as INT64.</returns>
    public static long GetChatId(this Update update)
    {
        return update.Message is null ? update.CallbackQuery.Message.Chat.Id : update.Message.Chat.Id;
    }
}