﻿using MessengerInsurance.Telegram.Extension;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;

namespace MessengerInsurance.Telegram.Services;

/// <summary>
///     A mock database for telegram users.
/// </summary>
public class TelegramServiceAppMemory : ITelegramService
{
    private static readonly IDictionary<long, TelegramUserModel> USERS = new Dictionary<long, TelegramUserModel>();

    /// <inheritdoc />
    public Task<TelegramUserModel?> GetUserAsync(long userChatId)
    {
        USERS.TryGetValue(userChatId, out TelegramUserModel? user);
        return Task.FromResult(user);
    }

    /// <inheritdoc />
    public async Task PatchUserAsync(long userChatId, TelegramUserModel userModel)
    {
        TelegramUserModel? userEntity = await GetUserAsync(userChatId);
        if (userEntity is null)
        {
            USERS[userChatId] = userModel;
        }
        else
        {
            USERS[userChatId] = userEntity.Patch(userModel);
        }
    }
}