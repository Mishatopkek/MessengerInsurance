﻿using System.Reflection;
using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Extension;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace MessengerInsurance.Telegram.Services;

/// <summary>
///     Dispatches Telegram commands to the appropriate handlers.
/// </summary>
public static class TelegramCommandDispatcher
{
    /// <summary>
    ///     The assembly in which the command handlers are located. It needs to create a Reply from this assembly
    /// </summary>
    private static readonly Assembly _assembly = typeof(TelegramCommandDispatcher).Assembly;

    /// <summary>
    ///     Dispatches a command to the appropriate handler.
    /// </summary>
    /// <param name="model">The name of the model that represents the command.</param>
    /// <param name="update">The update from Telegram that triggered the command.</param>
    /// <param name="botClient">The Telegram bot client.</param>
    /// <param name="isFirstInput">Indicates whether this is the first input from the other commands.</param>
    public static async Task Dispatch(
        string model,
        Update update,
        ITelegramBotClient botClient,
        bool isFirstInput)
    {
        long userChatId = update.GetChatId();

        try
        {
            //Create an instance of the Reply
            Type? type = _assembly.GetType(model);
            if (type == null)
                throw new ArgumentOutOfRangeException(nameof(update), "Unknown model type");

            TelegramReplyBase? telegramReply = CreateInstance(type, update, botClient);

            if (telegramReply == null)
                throw new ArgumentOutOfRangeException(nameof(update), "Unable to create instance of type");

            telegramReply.IsFirstInput = isFirstInput;

            //If it's a first input, we need to call the appropriate behavior of the Reply
            if (isFirstInput)
                await HandleFirstInputAsync(update, telegramReply);
            else
                await telegramReply.OnMessageAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            await botClient.SendTextMessageAsync(
                userChatId,
                "An error occurred while processing the command. Please try again later.");
        }
    }

    /// <summary>
    ///     Creates an instance of the appropriate command handler based on the type of the update.
    /// </summary>
    /// <param name="type">The type of the command handler to create.</param>
    /// <param name="update">The update from Telegram that triggered the command.</param>
    /// <param name="botClient">The Telegram bot client.</param>
    /// <returns>An instance of the appropriate command handler, or null if no suitable handler could be found.</returns>
    private static TelegramReplyBase? CreateInstance(Type type, Update update, ITelegramBotClient botClient)
    {
        return update.Type switch
        {
            UpdateType.Message => update.Message!.Type switch
            {
                MessageType.Text => Activator.CreateInstance(type, update.Message, botClient,
                    new TelegramServiceAppMemory()) as TelegramReplyBase,
                MessageType.Photo => Activator.CreateInstance(type, update.Message, botClient,
                    new TelegramServiceAppMemory()) as TelegramReplyBase,
                _ => null
            },
            UpdateType.CallbackQuery => Activator.CreateInstance(type, update.CallbackQuery!.Message, botClient,
                new TelegramServiceAppMemory()) as TelegramReplyBase,
            _ => null
        };
    }

    /// <summary>
    ///     Handles the first input from the user.
    /// </summary>
    /// <param name="update">The update from Telegram that triggered the command.</param>
    /// <param name="telegramReply">The command handler.</param>
    private static async Task HandleFirstInputAsync(Update update, TelegramReplyBase telegramReply)
    {
        switch (update.Type)
        {
            case UpdateType.Message:
                switch (update.Message!.Type)
                {
                    case MessageType.Text:
                        await telegramReply.OnMessageAsync();
                        break;
                    case MessageType.Photo:
                        await telegramReply.OnPhotoAsync();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(update), "Unknown message type");
                }

                break;

            case UpdateType.CallbackQuery:
                await telegramReply.OnButtonAsync(update.CallbackQuery!.Data!);
                break;

            default:
                throw new ArgumentOutOfRangeException(nameof(update), "Unknown update type");
        }
    }
}