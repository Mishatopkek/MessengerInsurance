﻿using MessengerInsurance.Telegram.Extension;
using MessengerInsurance.Telegram.Handlers;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MessengerInsurance.Telegram.Services;

/// <summary>
///     Represents the endpoint for the Telegram bot.
/// </summary>
public class TelegramEndpoint
{
    /// <summary>
    ///     Initializes a new instance of the TelegramEndpoint class.
    /// </summary>
    /// <param name="token">The token for the Telegram bot.</param>
    /// <param name="cancellationToken">A cancellation token that can be used to cancel the operation.</param>
    public TelegramEndpoint(string token, CancellationToken cancellationToken)
    {
        ITelegramBotClient telegramBotClient = new TelegramBotClient(token);
        telegramBotClient.StartReceiving(HandleUpdateAsync, HandleErrorAsync, cancellationToken: cancellationToken);
    }

    /// <summary>
    ///     Starts the Telegram bot.
    /// </summary>
    public void Start()
    {
        Console.WriteLine("Ready!");
        Thread.Sleep(int.MaxValue);
    }

    /// <summary>
    ///     Handles updates from the Telegram bot.
    /// </summary>
    private static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        Console.WriteLine("----------------------------------------");

        bool isFirstInput = true;
        ITelegramService
            telegramService = new TelegramServiceAppMemory(); //Can be any database. The current solution is in-memory
        string telegramUserState = await GetCurrentUserState(update, telegramService);
        string actualState = telegramUserState;
        do
        {
            telegramUserState = actualState;

            // Run the command where user is currently at
            await TelegramCommandDispatcher.Dispatch(telegramUserState, update, botClient, isFirstInput);

            // Compare the state before and after the command
            actualState = await GetCurrentUserState(update, telegramService);
            isFirstInput = false;
        } while (telegramUserState != actualState); //If the state has changed, run the new command
    }

    /// <summary>
    ///     Gets the current state of the user.
    /// </summary>
    private static async Task<string> GetCurrentUserState(Update update, ITelegramService telegramService)
    {
        long userChatId = update.GetChatId();
        TelegramUserModel? user = await telegramService.GetUserAsync(userChatId);
        string? telegramUserState =
            user?.CurrentTelegramUserState ?? typeof(WelcomeReply).FullName; //WelcomeReply is the default state
        LoggingUserAndState(userChatId, telegramUserState);
        return telegramUserState;
    }

    /// <summary>
    ///     Logs the user and their current state in the console.
    /// </summary>
    private static void LoggingUserAndState(long userChatId, string? telegramUserState)
    {
        Console.WriteLine("--------------------");
        Console.WriteLine("User: " + userChatId);
        Console.WriteLine("User state: " + telegramUserState);
    }

    /// <summary>
    ///     Handles errors from the Telegram bot.
    /// </summary>
    static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
    {
        Console.WriteLine($"An error occurred: {exception.Message}");
        return Task.CompletedTask;
    }
}