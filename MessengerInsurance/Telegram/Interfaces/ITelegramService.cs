﻿using Models_TelegramUserModel = MessengerInsurance.Telegram.Models.TelegramUserModel;

namespace MessengerInsurance.Telegram.Interfaces;

/// <summary>
///     A service to save user's data in a database. Currently, it can be understood as UserService
/// </summary>
public interface ITelegramService
{
    /// <summary>
    ///     Get Telegram user's data.
    /// </summary>
    /// <param name="userChatId">The chat ID of the user.</param>
    /// <returns>Contains the Telegram user's data.</returns>
    public Task<Models_TelegramUserModel?> GetUserAsync(long userChatId);

    /// <summary>
    ///     Updates a Telegram user's data.
    /// </summary>
    /// <param name="userChatId">The chat ID of the user.</param>
    /// <param name="userModel">The new data for the user.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    public Task PatchUserAsync(long userChatId, Models_TelegramUserModel userModel);
}