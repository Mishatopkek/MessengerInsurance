﻿using MessengerInsurance.Ocr.Models;

namespace MessengerInsurance.Telegram.Models;

/// <summary>
///     Represents a Telegram user.
/// </summary>
public class TelegramUserModel
{
    /// <summary>
    ///     Shows the current user's position among Reply classes. It's typeof class name and it's full name
    /// </summary>
    public string CurrentTelegramUserState { get; set; }

    /// <summary>
    ///     User's passport model
    /// </summary>
    public OcrPassportModel Passport { get; set; }

    /// <summary>
    ///     User's car model
    /// </summary>
    public OcrCarModel Car { get; set; }
}