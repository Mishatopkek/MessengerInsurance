namespace MessengerInsurance.Telegram.Abstractions.Attributes;

[AttributeUsage(AttributeTargets.Method, Inherited = false)]
public class ButtonAttribute(string name) : Attribute
{
    public string Name { get; } = name;
}