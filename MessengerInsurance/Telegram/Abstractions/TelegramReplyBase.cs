using System.Reflection;
using MessengerInsurance.Telegram.Abstractions.Attributes;
using MessengerInsurance.Telegram.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MessengerInsurance.Telegram.Abstractions;

/// <summary>
///     Base class for all telegram replies. Contains methods for handling messages, photos, and buttons.
/// </summary>
public abstract class TelegramReplyBase(Message userAction, ITelegramBotClient botClient, ITelegramService service)
{
    /// <summary>
    ///     The user's message info.
    /// </summary>
    internal Message UserAction { get; set; } = userAction;

    /// <summary>
    ///     The Telegram bot client from Telegram API.
    /// </summary>
    internal ITelegramBotClient BotClient { get; set; } = botClient;

    /// <summary>
    ///     The Telegram service.
    /// </summary>
    internal ITelegramService Service { get; set; } = service;

    /// <summary>
    ///     The user's chat ID as a string.
    /// </summary>
    internal string UserChatId => UserChatIdLong.ToString();

    /// <summary>
    ///     The user's chat ID as INT64.
    /// </summary>
    internal long UserChatIdLong => UserAction.Chat.Id;

    /// <summary>
    ///     Indicates whether this is the input from user. It can be false when you got here from another reply(command)
    /// </summary>
    public bool IsFirstInput { get; set; } = true;

    /// <summary>
    ///     It's a base input. User can get here from a simple text message
    /// </summary>
    public virtual async Task OnMessageAsync()
    {
        await BotClient.SendTextMessageAsync(UserChatIdLong, "Message is not supported yet");
        Console.WriteLine($"Message is not supported in {GetType().Name}");
    }

    /// <summary>
    ///     User can get here from sending an image
    /// </summary>
    public virtual async Task OnPhotoAsync()
    {
        await BotClient.SendTextMessageAsync(UserChatIdLong, "Photo is not supported yet");
        Console.WriteLine($"Photo is not supported in {GetType().Name}");
    }

    /// <summary>
    ///     If you want to add a button, you need to use attribute Button with the name of the button. <br />
    ///     This method needs to call a method with the same name as the button name.
    /// </summary>
    /// <param name="buttonName">Name of a button attribute</param>
    public async Task OnButtonAsync(string buttonName)
    {
        //Get all methods where Button attribute is set
        IEnumerable<MethodInfo> methods = GetType()
            .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
            .Where(m => m.GetCustomAttribute<ButtonAttribute>() != null);

        //Call the method with buttonName as parameter in the attribute
        foreach (MethodInfo method in methods)
        {
            ButtonAttribute? attribute = method.GetCustomAttribute<ButtonAttribute>();
            if (attribute.Name != buttonName) continue;
            if (method.ReturnType == typeof(Task))
                await (Task) method.Invoke(this, null)!;
            else
                method.Invoke(this, null);

            return;
        }

        //In case if there are no such buttons
        Console.WriteLine($"No method found with button name {buttonName} in {GetType().Name}");
        await OnMessageAsync();
    }
}