using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Abstractions.Attributes;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace MessengerInsurance.Telegram.Handlers;

/// <summary>
///     Send a message when user starts a conversation with the bot.
/// </summary>
/// <param name="userAction">The user's action message.</param>
/// <param name="botClient">The Telegram bot client.</param>
/// <param name="service">The Telegram service.</param>
public class WelcomeReply(Message userAction, ITelegramBotClient botClient, ITelegramService service)
    : TelegramReplyBase(userAction, botClient, service)
{
    private const string GET_QUOTE = "get_quote";

    /// <summary>
    ///     Sends a welcome message to the user
    /// </summary>
    public override async Task OnMessageAsync()
    {
        const string messageText =
            "Hello! I'm here to assist you with car insurance purchases. How can I help you today?";
        InlineKeyboardMarkup inlineKeyboard = new(new[]
        {
            new[]
            {
                InlineKeyboardButton.WithCallbackData("Get a Quote", GET_QUOTE)
            }
        });
        await BotClient.SendTextMessageAsync(UserChatId, messageText, replyMarkup: inlineKeyboard);
    }

    /// <summary>
    ///     When user clicks on the "Get a Quote" button, move to the next reply.
    /// </summary>
    [Button(GET_QUOTE)]
    public async Task OnGetQuoteButton()
    {
        await Service.PatchUserAsync(UserChatIdLong,
            new TelegramUserModel
                {CurrentTelegramUserState = typeof(PreparingToGetScanReply).FullName}); //Move to the next command
    }
}