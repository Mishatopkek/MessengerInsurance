﻿using MessengerInsurance.Llm.Interfaces;
using MessengerInsurance.Llm.Service;
using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MessengerInsurance.Telegram.Handlers;

/// <summary>
///     With LLM generates a dummy insurance policy document.
/// </summary>
/// <param name="userAction">The user's action message.</param>
/// <param name="botClient">The Telegram bot client.</param>
/// <param name="service">The Telegram service.</param>
public class InsurancePolicyReply(Message userAction, ITelegramBotClient botClient, ITelegramService service)
    : TelegramReplyBase(userAction, botClient, service)
{
    /// <summary>
    ///     Generates a dummy insurance policy document.
    /// </summary>
    public override async Task OnMessageAsync()
    {
        TelegramUserModel? user = await service.GetUserAsync(UserChatIdLong);
        await botClient.SendTextMessageAsync(UserChatIdLong,
            "It is now generates policy, it usually takes about a minute, please wait...");
        const string role = "You are a helpful assistant.";
        string context =
            "Generate a dummy insurance policy document. The only available price is $100. Here's extra information: "
            + user.CurrentTelegramUserState
            + user.Passport;
        ILllService llmService = new LllChatGptService();
        string message = await llmService.GenerateLlmPromptAsync(role,
            context);
        await botClient.SendTextMessageAsync(UserChatIdLong, message);
        await service.PatchUserAsync(UserChatIdLong,
            new TelegramUserModel {CurrentTelegramUserState = typeof(WelcomeReply).FullName});
    }
}