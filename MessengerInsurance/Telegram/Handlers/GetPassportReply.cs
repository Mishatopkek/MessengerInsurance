﻿using MessengerInsurance.Ocr.Interfaces;
using MessengerInsurance.Ocr.Services;
using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;
using File = Telegram.Bot.Types.File;

namespace MessengerInsurance.Telegram.Handlers;

/// <summary>
///     Ask user's passport photo.
/// </summary>
/// <param name="userAction">The user's action message.</param>
/// <param name="botClient">The Telegram bot client.</param>
/// <param name="service">The Telegram service.</param>
public class GetPassportReply(Message userAction, ITelegramBotClient botClient, ITelegramService service)
    : TelegramReplyBase(userAction, botClient, service)
{
    /// <summary>
    ///     Ask user's passport photo.
    /// </summary>
    public override async Task OnMessageAsync()
    {
        await botClient.SendTextMessageAsync(UserChatId, "Please, send a photo of your passport");
    }

    /// <summary>
    ///     Handle a passport photo
    /// </summary>
    public override async Task OnPhotoAsync()
    {
        string? fileId = UserAction.Photo[^1].FileId; // Get the highest resolution photo
        File? fileInfo = await botClient.GetFileAsync(fileId);

        IOcrService ocrService = new OcrMindeeService();
        TelegramUserModel? userModel = new();
        using (MemoryStream? memoryStream = new())
        {
            await botClient.DownloadFileAsync(fileInfo.FilePath, memoryStream);
            byte[] data = memoryStream.ToArray();
            userModel.Passport = await ocrService.PassportPhotoOcrAsync(data);
        }

        userModel.CurrentTelegramUserState = typeof(GetCarReply).FullName;
        await service.PatchUserAsync(UserChatIdLong, userModel);
    }
}