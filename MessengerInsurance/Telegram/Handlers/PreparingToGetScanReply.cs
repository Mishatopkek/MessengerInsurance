using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MessengerInsurance.Telegram.Handlers;

/// <summary>
///     Send a user a message that he is going to send a photo of his passport and his car id.
/// </summary>
/// <param name="userAction">The user's action message.</param>
/// <param name="botClient">The Telegram bot client.</param>
/// <param name="service">The Telegram service.</param>
public class PreparingToGetScanReply(Message userAction, ITelegramBotClient botClient, ITelegramService service)
    : TelegramReplyBase(userAction, botClient, service)
{
    /// <summary>
    ///     Send a message about preparing of his passport and car id photo.
    /// </summary>
    public override async Task OnMessageAsync()
    {
        await botClient.SendTextMessageAsync(UserChatIdLong,
            "You are going to send a photo of your passport and your car id");
        await service.PatchUserAsync(UserChatIdLong,
            new TelegramUserModel {CurrentTelegramUserState = typeof(GetPassportReply).FullName});
    }
}