﻿using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Abstractions.Attributes;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace MessengerInsurance.Telegram.Handlers;

/// <summary>
///     Send a warning about getting the price of the insurance.
/// </summary>
/// <param name="userAction">The user's action message.</param>
/// <param name="botClient">The Telegram bot client.</param>
/// <param name="service">The Telegram service.</param>
public class PriceWarningReply(Message userAction, ITelegramBotClient botClient, ITelegramService service)
    : TelegramReplyBase(userAction, botClient, service)
{
    private const string CONFIRM_YES = "confirm_yes";
    private const string CONFIRM_NO = "confirm_no";

    /// <summary>
    ///     Ask about agreement with the insurance price.
    /// </summary>
    public override async Task OnMessageAsync()
    {
        InlineKeyboardMarkup inlineKeyboard = new(new[]
        {
            new[]
            {
                InlineKeyboardButton.WithCallbackData("Yes, I agree", CONFIRM_YES),
                InlineKeyboardButton.WithCallbackData("No, I disagree", CONFIRM_NO)
            }
        });
        await botClient.SendTextMessageAsync(UserChatIdLong, "The insurance price is fixed 100 USD per year",
            replyMarkup: inlineKeyboard);
    }

    /// <summary>
    ///     When user pressed a button where the user agrees with the price.
    /// </summary>
    [Button(CONFIRM_YES)]
    public async Task OnConfirmYes()
    {
        await service.PatchUserAsync(UserChatIdLong,
            new TelegramUserModel {CurrentTelegramUserState = typeof(InsurancePolicyReply).FullName});
    }

    /// <summary>
    ///     When user pressed a button where the user disagrees with the price.
    /// </summary>
    [Button(CONFIRM_NO)]
    public async Task OnConfirmNo()
    {
        await botClient.SendTextMessageAsync(UserChatIdLong, "I'm so sorry, but it's the only available price");
        await OnMessageAsync();
    }
}