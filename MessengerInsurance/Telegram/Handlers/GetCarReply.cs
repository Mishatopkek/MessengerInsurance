﻿using MessengerInsurance.Ocr.Interfaces;
using MessengerInsurance.Ocr.Services;
using MessengerInsurance.Telegram.Abstractions;
using MessengerInsurance.Telegram.Interfaces;
using MessengerInsurance.Telegram.Models;
using Telegram.Bot;
using Telegram.Bot.Types;
using File = Telegram.Bot.Types.File;

namespace MessengerInsurance.Telegram.Handlers;

/// <summary>
///     Ask about user's car photo.
///     Inherits from the TelegramReplyBase class.
/// </summary>
/// <param name="userAction">The user's action message.</param>
/// <param name="botClient">The Telegram bot client.</param>
/// <param name="service">The Telegram service.</param>
public class GetCarReply(Message userAction, ITelegramBotClient botClient, ITelegramService service)
    : TelegramReplyBase(userAction, botClient, service)
{
    /// <summary>
    ///     Ask about user's car photo.
    /// </summary>
    public override async Task OnMessageAsync()
    {
        await botClient.SendTextMessageAsync(UserChatIdLong, "Please, send a photo of your car");
    }

    /// <summary>
    ///     Handle the photo of the user's car.
    /// </summary>
    public override async Task OnPhotoAsync()
    {
        string fileId = UserAction.Photo![^1].FileId; // Get the highest resolution photo
        File fileInfo = await botClient.GetFileAsync(fileId);

        IOcrService ocrService = new OcrMindeeService(); //Use ocrService. In the test tests, we use Mindee OCR service
        TelegramUserModel userModel = new();

        using (MemoryStream memoryStream = new())
        {
            await botClient.DownloadFileAsync(fileInfo.FilePath, memoryStream);
            byte[] data = memoryStream.ToArray();
            userModel.Car = await ocrService.CarPhotoOcrAsync(data);
        }

        userModel.CurrentTelegramUserState = typeof(PriceWarningReply).FullName; //Move to the next command
        await service.PatchUserAsync(UserChatIdLong, userModel);
    }
}