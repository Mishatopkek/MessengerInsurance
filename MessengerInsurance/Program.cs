using MessengerInsurance.Telegram.Services;

using CancellationTokenSource token = new();

//Start the Telegram bot
TelegramEndpoint endpoint = new(Environment.GetEnvironmentVariable("TELEGRAM_API_KEY"), token.Token);

//Sleep the thread, just to make the bot is running in the background
endpoint.Start();