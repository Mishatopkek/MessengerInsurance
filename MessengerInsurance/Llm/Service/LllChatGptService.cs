﻿using System.Text;
using MessengerInsurance.Llm.Interfaces;
using Newtonsoft.Json.Linq;

namespace MessengerInsurance.Llm.Service;

/// <summary>
///     LLM based on ChatGpt from OpenAI.
///     Implements the <see cref="ILllService" /> interface.
/// </summary>
public class LllChatGptService : ILllService
{
    private static string? openAiApiKey = Environment.GetEnvironmentVariable("OPENAI_API_KEY");
    private static readonly HttpClient client = new();

    /// <inheritdoc />
    public async Task<string> GenerateLlmPromptAsync(string role, string content)
    {
        const string apiUrl = "https://api.openai.com/v1/chat/completions";

        JObject requestBody = new()
        {
            ["model"] = "gpt-4-turbo-2024-04-09",
            ["messages"] = new JArray
            {
                new JObject {["role"] = "system", ["content"] = role},
                new JObject {["role"] = "user", ["content"] = content}
            }
        };

        using HttpRequestMessage request = new(HttpMethod.Post, apiUrl);
        request.Content = new StringContent(requestBody.ToString(), Encoding.UTF8, "application/json");
        request.Headers.Add("Authorization", $"Bearer {openAiApiKey}");

        try
        {
            HttpResponseMessage response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error: {response.StatusCode}");
                throw new HttpRequestException(response.StatusCode.ToString());
            }

            // Read the response content as a string
            string stringResponse = await response.Content.ReadAsStringAsync();
            JObject jsonObject = JObject.Parse(stringResponse);
            string text = jsonObject["choices"][0]["message"]["content"].ToString();
            Console.WriteLine(text);
            return text;
        }
        catch (HttpRequestException e)
        {
            Console.WriteLine($"\nException Caught!\nMessage :{e.Message}");
            throw;
        }
    }
}