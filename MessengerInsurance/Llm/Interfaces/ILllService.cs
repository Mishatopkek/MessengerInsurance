﻿namespace MessengerInsurance.Llm.Interfaces;

/// <summary>
///     Interface for the LLM Service.
/// </summary>
public interface ILllService
{
    /// <summary>
    ///     Generates a LLM Prompt
    /// </summary>
    /// <param name="role">The role for which the prompt is generated.</param>
    /// <param name="content">The content to be included in the prompt.</param>
    /// <returns>
    ///     A Task that represents the asynchronous operation.
    ///     The task result contains the generated LLM prompt.
    /// </returns>
    public Task<string> GenerateLlmPromptAsync(string role, string content);
}