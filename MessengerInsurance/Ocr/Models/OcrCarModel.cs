﻿namespace MessengerInsurance.Ocr.Models;

/// <summary>
///     Represents the OCR model of a car.
/// </summary>
public class OcrCarModel
{
    /// <summary>
    ///     Plate number of the car.
    /// </summary>
    public string Id { get; set; }
}