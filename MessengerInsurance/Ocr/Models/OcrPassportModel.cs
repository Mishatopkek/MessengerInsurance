﻿using System.Text;

namespace MessengerInsurance.Ocr.Models;

/// <summary>
///     Represents the OCR model of a foreign passport.
/// </summary>
public class OcrPassportModel
{
    /// <summary>
    /// The first name of the passport holder.
    /// </summary>
    public string FirstName { get; set; }

    /// <summary>
    /// The last name of the passport holder.
    /// </summary>
    public string LastName { get; set; }

    /// <summary>
    /// The first name and last name of the passport holder.
    /// </summary>
    public string FullName => "" + FirstName + " " + LastName;

    /// <summary>
    /// The date of birth of the passport holder.
    /// </summary>
    public DateOnly BirthDate { get; set; }

    /// <summary>
    /// The place of birth of the passport holder.
    /// </summary>
    public string BirthPlace { get; set; }

    /// <summary>
    /// The country's 3-letter codes (ISO 3166-1 alpha-3).
    /// </summary>
    public string Country { get; set; }

    /// <summary>
    /// The expiry date of the passport.
    /// </summary>
    public DateOnly ExpiryDate { get; set; }

    /// <summary>
    /// The gender of the passport holder.
    /// </summary>
    public string Gender { get; set; }

    /// <summary>
    /// The passport's identification number.
    /// </summary>
    public string IdNumber { get; set; }

    /// <summary>
    /// The date the passport was issued.
    /// </summary>
    public DateOnly IssuanceDate { get; set; }

    /// <summary>
    /// Machine Readable Zone, first line
    /// </summary>
    public string Mrz1 { get; set; }

    /// <summary>
    /// Machine Readable Zone, second line
    /// </summary>
    public string Mrz2 { get; set; }

    /// <summary>
    /// A prettier representation of the current model values.
    /// </summary>
    public override string ToString()
    {
        StringBuilder result = new();
        result.Append($":Country Code: {Country}\n");
        result.Append($":ID Number: {IdNumber}\n");
        result.Append($":FirstName: {FirstName}\n");
        result.Append($":Lastname: {LastName}\n");
        result.Append($":Date of Birth: {BirthDate}\n");
        result.Append($":Place of Birth: {BirthPlace}\n");
        result.Append($":Gender: {Gender}\n");
        result.Append($":Date of Issue: {IssuanceDate}\n");
        result.Append($":Expiry Date: {ExpiryDate}\n");
        result.Append($":MRZ Line 1: {Mrz1}\n");
        result.Append($":MRZ Line 2: {Mrz2}\n");
        return result.ToString();
    }
}