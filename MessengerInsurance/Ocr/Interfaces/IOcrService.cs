﻿using MessengerInsurance.Ocr.Models;

namespace MessengerInsurance.Ocr.Interfaces;

/// <summary>
///     Interface for the OcrService.
/// </summary>
public interface IOcrService
{
    /// <summary>
    ///     Performs OCR on a foreign passport photo.
    /// </summary>
    /// <param name="file">The byte array of the passport photo file.</param>
    /// <returns>Contains the OCR result as an OcrPassportModel.</returns>
    public Task<OcrPassportModel> PassportPhotoOcrAsync(byte[] file);

    /// <summary>
    ///     Performs OCR on a car plate photo asynchronously.
    /// </summary>
    /// <param name="file">The byte array of the car plate photo file.</param>
    /// <returns>Contains the OCR result as an OcrCarModel.</returns>
    public Task<OcrCarModel> CarPhotoOcrAsync(byte[] file);
}