﻿using MessengerInsurance.Ocr.Interfaces;
using MessengerInsurance.Ocr.Models;
using Mindee;
using Mindee.Input;
using Mindee.Parsing.Common;
using Mindee.Product.Eu.LicensePlate;
using Mindee.Product.Passport;

namespace MessengerInsurance.Ocr.Services;

/// <summary>
///     Ocr service that uses the Mindee API to perform OCR.
/// </summary>
public class OcrMindeeService : IOcrService
{
    private static string? apiKey = Environment.GetEnvironmentVariable("MINDEE_API_KEY");

    /// <inheritdoc />
    public async Task<OcrPassportModel> PassportPhotoOcrAsync(byte[] file)
    {
        // Construct a new client
        MindeeClient mindeeClient = new(apiKey);

        // Load an input source as a path string
        // Other input types can be used, as mentioned in the docs
        LocalInputSource inputSource = new(file, "abc.png");

        // Call the API and parse the input
        PredictResponse<PassportV1>? response = await mindeeClient
            .ParseAsync<PassportV1>(inputSource);

        // Print a summary of all the predictions
        Console.WriteLine(response.Document.Inference.Prediction.ToString());
        OcrPassportModel passportModel = new()
        {
            FirstName = response.Document.Inference.Prediction.GivenNames[0].Value,
            LastName = response.Document.Inference.Prediction.Surname.Value,
            BirthDate = DateOnly.FromDateTime((DateTime) response.Document.Inference.Prediction.BirthDate.DateObject!),
            BirthPlace = response.Document.Inference.Prediction.BirthPlace.Value,
            Country = response.Document.Inference.Prediction.Country.Value,
            ExpiryDate =
                DateOnly.FromDateTime((DateTime) response.Document.Inference.Prediction.ExpiryDate.DateObject!),
            Gender = response.Document.Inference.Prediction.Gender.Value,
            IdNumber = response.Document.Inference.Prediction.IdNumber.Value,
            IssuanceDate =
                DateOnly.FromDateTime((DateTime) response.Document.Inference.Prediction.IssuanceDate.DateObject!),
            Mrz1 = response.Document.Inference.Prediction.Mrz1.Value,
            Mrz2 = response.Document.Inference.Prediction.Mrz2.Value
        };
        return passportModel;
    }

    /// <inheritdoc />
    public async Task<OcrCarModel> CarPhotoOcrAsync(byte[] file)
    {
        // Construct a new client
        MindeeClient mindeeClient = new(apiKey);

        // Load an input source as a path string
        // Other input types can be used, as mentioned in the docs
        LocalInputSource inputSource = new(file, "abc.png");

        // Call the API and parse the input
        PredictResponse<LicensePlateV1>? response = await mindeeClient
            .ParseAsync<LicensePlateV1>(inputSource);

        // Print a summary of all the predictions
        Console.WriteLine(response.Document.Inference.Prediction.ToString());
        OcrCarModel carModel = new()
        {
            Id = response.Document.Inference.Prediction.LicensePlates[0].Value
        };
        return carModel;
    }
}